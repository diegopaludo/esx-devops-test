FROM python:3.7.4

WORKDIR /app

COPY app/requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY app/api.py .

EXPOSE 8000

CMD [ "gunicorn", "--log-level", "debug", "api:app" ]
