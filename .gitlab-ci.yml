.build-common:
  image: docker:latest
  stage: publish
  services:
    - docker:dind
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY

.deploy-common:
  stage: deploy
  before_script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - DATE=$(date --utc +%FT%H%M%SZ)

# ----------------------------------------------------------------------------------------------------------------------

stages:
  - test
  - publish
  - deploy
  - migrations

include:
  - template: Security/Secret-Detection.gitlab-ci.yml
  - template: Security/SAST.gitlab-ci.yml
  - template: Code-Quality.gitlab-ci.yml

secret_detection:
  rules:
    - if: $CI_COMMIT_BRANCH == "development" || $CI_COMMIT_BRANCH == "staging" || $CI_COMMIT_BRANCH == "production"
      when: never
    - if: $CI_COMMIT_BRANCH
  artifacts:
    paths: [gl-secret-detection-report.json]
  tags:
    - docker-runner

semgrep-sast:
  artifacts:
    paths: [gl-sast-report.json]
  tags:
    - docker-runner

code_quality:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_COMMIT_BRANCH == "development" || $CI_COMMIT_BRANCH == "staging" || $CI_COMMIT_BRANCH == "production"
  tags:
    - docker-runner

code_quality_html:
  extends: code_quality
  variables:
    REPORT_FORMAT: html
  artifacts:
    paths: [gl-code-quality-report.html]

# ----------------------------------------------------------------------------------------------------------------------

build-image:
  extends: .build-common
  script:
    - docker build -f "./Dockerfile" --pull -t "$CI_REGISTRY_IMAGE:$CI_COMMIT_BRANCH" .
    - docker push "$CI_REGISTRY_IMAGE:$CI_COMMIT_BRANCH"
  rules:
    - if: $CI_COMMIT_BRANCH == "development" || $CI_COMMIT_BRANCH == "staging"
  environment:
    name: $CI_COMMIT_BRANCH
  tags:
    - "$CI_COMMIT_BRANCH-docker-runner"

build-image-prod:
  extends: .build-common
  script:
    - docker build -f "./Dockerfile" --pull -t "$CI_REGISTRY_IMAGE:$CI_COMMIT_TAG" .
    - docker push "$CI_REGISTRY_IMAGE:$CI_COMMIT_TAG"
  rules:
    - if: $CI_COMMIT_TAG != null
  environment:
    name: production
  tags:
    - production-docker-runner

# ----------------------------------------------------------------------------------------------------------------------

deploy:
  extends: .deploy-common
  script:
    - NAMESPACE=$(grep -i "applicationNamespace:" chart/$CI_COMMIT_BRANCH-values.yaml | cut -c 23-)
    - HELM_APP_NAME=$(grep -i "name" chart/Chart.yaml | cut -c 7-)
    - APP_URL=$(grep -i "applicationDomain" chart/$CI_COMMIT_BRANCH-values.yaml | cut -c 20-)
    - echo APP_URL=$APP_URL >> deploy.env
    - echo K8S_NAMESPACE=$NAMESPACE >> deploy.env
    - sed -i "s/__APP_VERSION__/$CI_COMMIT_BRANCH/g" $CI_PROJECT_DIR/chart/Chart.yaml
    - kubectl config use-context $CI_PROJECT_PATH:$CI_COMMIT_BRANCH
    - helm upgrade $HELM_APP_NAME $CI_PROJECT_DIR/chart --install --namespace $NAMESPACE --values $CI_PROJECT_DIR/chart/$CI_COMMIT_BRANCH-values.yaml --set "applicationImage=$CI_REGISTRY_IMAGE:$CI_COMMIT_BRANCH" --set "applicationVersion=$CI_COMMIT_BRANCH" --set "applicationDeployDate=$DATE"
  artifacts:
    reports:
      dotenv: deploy.env
  rules:
    - if: $CI_COMMIT_BRANCH == "development" || $CI_COMMIT_BRANCH == "staging"
  environment:
    name: $CI_COMMIT_BRANCH
    url: https://$APP_URL
    kubernetes:
      namespace: $K8S_NAMESPACE
  tags:
    - "$CI_COMMIT_BRANCH-docker-bash"

deploy-prod:
  extends: .deploy-common
  script:
    - NAMESPACE=$(grep -i "applicationNamespace:" chart/production-values.yaml | cut -c 23-)
    - HELM_APP_NAME=$(grep -i "name" chart/Chart.yaml | cut -c 7-)
    - APP_URL=$(grep -i "applicationDomain" chart/production-values.yaml | cut -c 20-)
    - echo APP_URL=$APP_URL >> deploy.env
    - echo K8S_NAMESPACE=$NAMESPACE >> deploy.env
    - sed -i "s/__APP_VERSION__/$CI_COMMIT_TAG/g" $CI_PROJECT_DIR/chart/Chart.yaml
    - kubectl config use-context $CI_PROJECT_PATH:production
    - helm upgrade $HELM_APP_NAME $CI_PROJECT_DIR/chart --install --namespace $NAMESPACE --values $CI_PROJECT_DIR/chart/production-values.yaml --set "applicationImage=$CI_REGISTRY_IMAGE:$CI_COMMIT_TAG" --set "applicationVersion=$CI_COMMIT_TAG" --set "applicationDeployDate=$DATE"
  artifacts:
    reports:
      dotenv: deploy.env
  rules:
    - if: $CI_COMMIT_TAG != null
  environment:
    name: production
    url: https://$APP_URL
    kubernetes:
      namespace: $K8S_NAMESPACE
  tags:
    - production-docker-bash
