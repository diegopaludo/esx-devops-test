# Comentários sobre o teste

Assumindo que estamos usando a cloud AWS com contas separadas por ambiente.

Uma conta para **development** outra para **staging** e uma terceira para **procuction**.

Tais contas devidamente configuradas e com cluster kubernetes operacional e usando o Kong como Ingress Controller. E com o agent do datadog instalado no cluster.

Para pipeline de CI/CD `.gitlab-ci.yml`, o repositório deverá ter as branches "development", "staging" e "production". Tais branches são associadas as contas com AWS. Um merge request aprovados para essas branches de **development** e **staging**, inicializaram a pipeline que criará a imagem docker e publicará a imagem no projeto do GitLab e em seguida o deploy fará o pull da imagem no cluster k8s da respectiva conta.

No caso de **procuction** teverá ser criada uma TAG com base na branch de **procuction** para o mesmo processo de entrega acontecer.

O monitoramento é baseado no Datadog com seu agent instalado a nível de cluster e as configurações na aplicação via injeção da lib do Datadog.
